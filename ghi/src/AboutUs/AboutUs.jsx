import AhadPhoto from "./AhadPhoto.jpg";
import JasminePhoto from "./JasminePhoto.png";
import ArtemisPhoto from "./ArtemisPhoto.jpg";
import EliPhoto from "./EliPhoto.jpg";
import linkedin from "./linkedin.png";

const people = [
	{
		name: "Ahad Ali",
		role: "Software Developer",
		imageUrl: AhadPhoto,
		linkedin: "linkedin.com/in/ahadali153",
		email: "ahadali153@gmail.com",
	},
	{
		name: "Chun Liu",
		role: "Software Developer",
		imageUrl: ArtemisPhoto,
		linkedin: "linkedin.com/in/chun-liu97/",
		email: "chunliu89@outlook.com",
	},
	{
		name: "Jasmine Gilson",
		role: "Software Developer",
		imageUrl: JasminePhoto,
		linkedin: "linkedin.com/in/jasmine-gilson/",
		email: "gilsonjasmine@gmail.com",
	},
	{
		name: "Eli Spicer",
		role: "Software Developer",
		imageUrl: EliPhoto,
		linkedin: "linkedin.com/in/elispicer/",
		email: "spicefe@gmail.com",
	},
];

export default function AboutUs() {
	return (
		<div className="bg-white py-24 sm:py-32">
			<div className="mx-auto grid max-w-7xl gap-x-8 gap-y-20 px-6 lg:px-8 xl:grid-cols-3">
				<div className="max-w-2xl">
					<h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">
						Meet our leadership
					</h2>
					<p className="mt-6 text-lg leading-8 text-gray-600">
						Libero fames augue nisl porttitor nisi, quis. Id ac elit odio vitae
						elementum enim vitae ullamcorper suspendisse.
					</p>
				</div>
				<ul
					role="list"
					className="grid gap-x-8 gap-y-12 sm:grid-cols-2 sm:gap-y-16 xl:col-span-2"
				>
					{people.map((person) => (
						<li key={person.name}>
							<div className="flex items-center gap-x-6">
								<img
									className="h-16 w-16 rounded-full"
									src={person.imageUrl}
									alt=""
								/>
								<div>
									<h3 className="text-base font-semibold leading-7 tracking-tight text-gray-900">
										{person.name}
									</h3>
									<p className="text-sm font-semibold leading-6 text-indigo-600">
										{person.role}
									</p>
									<h3 className="text-base font-semibold leading-7 tracking-tight text-gray-900">
										{person.email}
									</h3>
									<a href={person.linkedin}>{linkedin}</a>
								</div>
							</div>
						</li>
					))}
				</ul>
			</div>
		</div>
	);
}
